# Facial Emotion Recognition using Convolutional Neural Networks(CNNs)

## Getting Started

These instructions will get this model up and running. Follow them to make use of the `classify_image.py` file to recognize facial emotions using custom images or `webcam.py` file to identify emotions on a live feed from your webcam.

### Dataset

Download the dataset from the given link:   
	https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/data   

Keep the `.csv` file inside the `dataset` folder.

### Prerequisites

Install these prerequisites before proceeding :-

* pip3 install tensorflow
* pip3 install keras
* pip3 install numpy
* pip3 install sklearn
* pip3 install pandas
* pip3 install opencv-python
---

## Getting accuracy of a model

Run the `models_test_accuracy.py` file with appropriate model imports uncommented to display the accuracy of the model.
```python
json_file = open('./model-weights/model1_deep_100-epochs.json', 'r')
# json_file = open('./model-weights/model2_shallow_100-epochs.json', 'r')
# json_file = open('./model-weights/model2_shallow_200-epochs.json', 'r')
# json_file = open('./model-weights/model3_100-epochs.json', 'r')

loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)

# load weights into new model
loaded_model.load_weights("./model-weights/model1_deep_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model2_shallow_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model2_shallow_200-epochs.h5")
# loaded_model.load_weights("./model-weights/model3_100-epochs.h5")
print("Loaded model from disk")
```

---
## Getting the Confusion Matrix & Classification Report

Run the `conf_matrix__classification_report.py` file with appropriate model imports uncommented (refer code snippet above) to display the confusion matrix & classification report of the model.

---