import cv2
import numpy as np
from time import sleep
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from keras.models import model_from_json

# classifier = load_model('./model-weights/model2_shallow_200-epochs.h5')

#loading the model
json_file = open('./model-weights/model1_deep_100-epochs.json', 'r')
# json_file = open('./model-weights/model1_deep_100-epochs.json', 'r')
# json_file = open('./model-weights/model1_deep_100-epochs.json', 'r')
# json_file = open('./model-weights/model3_100-epochs.json', 'r')

loaded_model_json = json_file.read()
json_file.close()
classifier = model_from_json(loaded_model_json)
# load weights into new model

classifier.load_weights("./model-weights/model1_deep_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model1_deep_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model1_deep_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model3_100-epochs.h5")

print("Loaded model from disk")

class_labels = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']

face_classifier = cv2.CascadeClassifier('./opencv_haarcascade/haarcascade_frontalface_default.xml')

def face_detector(img):
    # Convert image to grayscale
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    faces = face_classifier.detectMultiScale(gray, 1.3, 5)
    if faces is ():
        return (0,0,0,0), np.zeros((48,48), np.uint8), img
    
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]

    try:
        roi_gray = cv2.resize(roi_gray, (48, 48), interpolation = cv2.INTER_AREA)
    except:
        return (x,w,y,h), np.zeros((48,48), np.uint8), img
    return (x,w,y,h), roi_gray, img

cap = cv2.VideoCapture(0)

while True:

    ret, frame = cap.read()
    rect, face, image = face_detector(frame)
    if np.sum([face]) != 0.0:
        roi = face.astype("float") / 255.0
        roi = img_to_array(roi)
        roi = np.expand_dims(roi, axis=0)

        # make a prediction on the ROI, then lookup the class
        preds = classifier.predict(roi)[0]
        label = class_labels[preds.argmax()]  
        label_position = (rect[0] + int((rect[1]/2)), rect[2] + 25)
        cv2.putText(image, label, label_position , cv2.FONT_HERSHEY_SIMPLEX,2, (0,255,0), 3)
    else:
        cv2.putText(image, "No Face Found", (20, 60) , cv2.FONT_HERSHEY_SIMPLEX,2, (0,255,0), 3)
        
    cv2.imshow('All', image)
    if cv2.waitKey(1) == 13: #13 is the Enter Key
        break
        
cap.release()
cv2.destroyAllWindows()