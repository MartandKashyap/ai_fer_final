# Visualize training history
from keras.models import Sequential
from keras.layers import Dense
from keras.models import load_model
from keras.models import model_from_json
from keras.utils import plot_model
from sklearn.metrics import classification_report, confusion_matrix, f1_score
import matplotlib.pyplot as plt
import numpy as np

# json_file = open('./model-weights/model1_deep_100-epochs.json', 'r')
# json_file = open('./model-weights/model2_shallow_100-epochs.json', 'r')
# json_file = open('./model-weights/model2_shallow_200-epochs.json', 'r')
json_file = open('./model-weights/model3_100-epochs.json', 'r')

loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)

# load weights into new model
# loaded_model.load_weights("./model-weights/model1_deep_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model2_shallow_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model2_shallow_200-epochs.h5")
loaded_model.load_weights("./model-weights/model3_100-epochs.h5")
print("Loaded model from disk")

truey = np.load('./temp-files/truey.npy')
predy = np.load('./temp-files/predy.npy')
labels = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']

print('Confusion Matrix')
cm = confusion_matrix(truey, predy)
print(cm)

print('Classification Report')
report = classification_report(truey, predy, target_names=list(labels))
print(report)