# load json and create model
from __future__ import division
from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy
import os
import numpy as np

# json_file = open('./model-weights/model1_deep_100-epochs.json', 'r')
# json_file = open('./model-weights/model2_shallow_100-epochs.json', 'r')
# json_file = open('./model-weights/model2_shallow_200-epochs.json', 'r')
json_file = open('./model-weights/model3_100-epochs.json', 'r')

loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)

# load weights into new model
# loaded_model.load_weights("./model-weights/model1_deep_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model2_shallow_100-epochs.h5")
# loaded_model.load_weights("./model-weights/model2_shallow_200-epochs.h5")
loaded_model.load_weights("./model-weights/model3_100-epochs.h5")
print("Loaded model from disk")

truey=[]
predy=[]
x = np.load('./temp-files/modXtest.npy')
y = np.load('./temp-files/modytest.npy')

yhat= loaded_model.predict(x)
yh = yhat.tolist()
yt = y.tolist()
count = 0

for i in range(len(y)):
    yy = max(yh[i])
    yyt = max(yt[i])
    predy.append(yh[i].index(yy))
    truey.append(yt[i].index(yyt))
    if(yh[i].index(yy)== yt[i].index(yyt)):
        count+=1

acc = (count/len(y))*100

#saving values for confusion matrix and analysis
np.save('./temp-files/truey', truey)
np.save('./temp-files/predy', predy)
print("Predicted and true label values saved")
print("Accuracy on test set :"+str(acc)+"%")
